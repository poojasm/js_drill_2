function problem2(arrayOfObjects, age) {
  try {
    /*----------using loops----------- */
    // const hobbies = [];

    // for(let index = 0; index < arrayOfObjects.length; index++) {
    // const {hobbies: userHobbies, age: userAge} = arrayOfObjects[index];

    // if(userAge === age) {
    //     hobbies.push(...userHobbies)
    // }
    // }
    // return hobbies;
    /*---------------using HOF----------- */
    const ageSpecificUser = arrayOfObjects.filter((user) => {
      return user.age == age;
    });

    const userHobbies = ageSpecificUser.map((user) => {
      return user.hobbies;
    });
    return userHobbies.flat();
  } catch (error) {
    console.log("Error");
  }
}

export default problem2;
