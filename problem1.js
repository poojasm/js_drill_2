function problem1(arrayOfObjects) {
  try {
    // const emails = [];

    /*-----------------Using Loops--------------*/
    // for (let index = 0; index < arrayOfObjects.length; index++) {
    // emails.push(arrayOfObjects[index].email);
    // }

    /*----------------using higher order functions----------*/
    const emails = arrayOfObjects.map(user => {
      return user.email;
    });
    return emails;
  } catch (error) {
    console.log('Error');
  }
}

export default problem1;
