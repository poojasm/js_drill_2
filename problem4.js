function problem4(arrayOfObjects, idx) {
  try {
    // const { name, city } = arrayOfObjects[idx];
    // return { name, city };

    /*----------using HOF--------- */
    const { name, city } = arrayOfObjects.at(idx);
    return { name, city };
  } catch (error) {
    console.log("Error");
  }
}

export default problem4;
