import problem4 from "./problem4.js";

function problem5(arrayOfObjects) {
  try {
    /*--------using loops---------- */
    // const ages = [];

    // for (let index = 0; index < arrayOfObjects.length; index++) {
    // ages.push(arrayOfObjects[index].age);
    // }
    // return ages
    /*-----------------Using HOF---------- */
    const agesOfUsers = arrayOfObjects.map((user) => {
      return user.age;
    });
    return agesOfUsers;
  } catch (error) {
    console.log("Error");
  }
}

export default problem5;
