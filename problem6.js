function problem6(arrayOfObjects) {
  try {
    /*--------------using loops--------- */
    // for (let index = 0; index < arrayOfObjects.length; index++) {
    // console.log(arrayOfObjects[index].hobbies[0]);
    //}
    /*--------------using HOF----------- */
    arrayOfObjects.forEach((user) => {
      console.log(user.hobbies[0]);
    });
  } catch (error) {
    console.log("Error");
  }
}

export default problem6;
