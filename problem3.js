function problem3(arrayOfObjects, passedCountry) {
  try {
    /*------------using loops--------- */
    // const names = [];

    // for (let index = 0; index < arrayOfObjects.length; index++) {
    // const { isStudent, country, name } = arrayOfObjects[index];

    // if (isStudent && country === passedCountry) {
    //   names.push(name);
    // }
    // }
    // return names;
    /*------------Using HOF--------- */
    const userInCountryName = arrayOfObjects.filter((user) => {
      return (
        user.isStudent &&
        user.country.toLowerCase() == passedCountry.toLowerCase()
      );
    });
    const studentInPassedCountry = userInCountryName.map((user) => {
      return user.name;
    });
    return studentInPassedCountry;
  } catch (error) {
    console.log("Error");
  }
}

export default problem3;
