function problem7(arrayOfObjects, age) {
  try {
    /*-----------using loops---------- */
    // for(let index = 0; index < arrayOfObjects.length; index++) {
    // const {name, age: userAge} = arrayOfObjects[index];

    // if(userAge === age) {
    //     console.log(`${name} : ${age}`)
    // }
    // }

    /*-----using HOF--------- */
    const userOfSpecificAge = arrayOfObjects.filter(user => {
      return user.age == age;
    });

    const nameAndEmail = userOfSpecificAge.map(user => {
      return [user.name, user.email];
    });
    return nameAndEmail;
  } catch (error) {
    console.log('Error');
  }
}

export default problem7;
