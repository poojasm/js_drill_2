import arrayOfObjects from "./data.js";
import problem4 from "../problem4.js";

// Name and city of individual at index position 3 in dataset.
const result = problem4(arrayOfObjects, 3);
console.log("🚀 ~ result:", result)
