function problem8(arrayOfObjects) {
  try {
    /*----------using loops------- */
    // for (let index = 0; index < arrayOfObjects.length; index++) {
    // const { city, country } = arrayOfObjects[index];
    // console.log(`${city} : ${country}`);
    // }

    /*--------using HOF--------- */
    arrayOfObjects.forEach((user) => {
      console.log([user.city, user.country]);
    });
  } catch (error) {
    console.log("Error");
  }
}

export default problem8;
